
import pstats
from sys import path
from os.path import join

perfpath = join(path[0], 'performance', '0-perf')
s = pstats.Stats(perfpath)
c = s.sort_stats('time')
c.print_stats(25)


