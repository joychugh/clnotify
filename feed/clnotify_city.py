#!/usr/bin/env python
from clnotify_log import pickle_load

def _get_cities():
    cities = [
        'nyc',
        'boston'
    ]
    return cities

def get_cities():
    return pickle_load('city_pickle')