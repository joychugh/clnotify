"""
Logging and Pickle
"""
import sys
import traceback
from os import linesep
from time import gmtime, strftime
import cPickle as pickle
from os import path as ospath, mkdir


def create_log_dir(path):
    """
    Create Log directory if It does not exist
    """
    try:
        if not ospath.exists(path):
            mkdir(path)
    except OSError:
        print "Can't create a LOG directory, please start as Administrator or SUDO"
        print traceback.format_exception(*sys.exc_info())


def write_log(logfile, content, additional=''):
    """
    Append data to log file
    @param logfile: name of the log file
    @type logfile: string
    @param content: log message
    @type content: string
    @param additional: any additional information
    @type additional: strings
    """
    logentry = "{time} | {log} | {add}\n"
    logentry = logentry.format(time=strftime('%d-%b-%Y %H:%M:%S', gmtime()), log=content, add=additional)
    path = "{base}\\logs\\{filename}.log".format(base=sys.path[0], filename=logfile)
    try:
        log = open(path, 'a')
        try:
            log.write(logentry)
            log.write(linesep)
        finally:
            log.close()
    except IOError as ioex:
        print "Error in Log: {0}".format(str(ioex))


def write_exception_log(exception, logfile):
    """
    Accepts a tupple
    (exc_type, exc_value, exc_traceback)
    and creates a meaniingful message
    """
    # Send the exception tuple to format_Exception using *
    # Expanding tuple by *
    message = traceback.format_exception(*exception)
    message = ''.join(line for line in message)
    path = "{base}\\logs\\{filename}_exception".format(base=sys.path[0], filename=logfile)
    try:
        log = open(path, 'a')
        try:
            log.write("Error On:{0}".format(strftime('%d-%b-%Y %H:%M:%S', gmtime())))
            log.write(message)
            log.write(linesep)
        finally:
            log.close()
    except IOError as ioex:
        print "Error in Log: {0}".format(str(ioex))


def pickle_dump(p_object, filename, append=False):
    """
    Pickle p_object to a file name
    """
    path = "{base}\\logs\\{filename}.pickle".format(base=sys.path[0], filename=filename)
    try:
        if append:
            p_file = open(path, 'ab')
        else:
            p_file = open(path, 'wb')
        try:
            pickled = pickle.dump(p_object, p_file)
        finally:
            p_file.close()
    except (IOError, EOFError, pickle.PickleError):
        write_exception_log(sys.exc_info(), 'pickle_error')
        return False
    return pickled


def pickle_load(filename):
    """
    Read a pickle file
    """
    path = "{base}\\logs\\{filename}.pickle".format(base=sys.path[0], filename=filename)
    try:
        p_file = open(path, 'rb')
        try:
            pickled = pickle.load(p_file)
        finally:
            p_file.close()
    except (IOError, EOFError, pickle.PickleError):
        write_exception_log(sys.exc_info(), 'pickle_error')
        return False
    return pickled
