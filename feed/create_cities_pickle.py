import urllib2
from lxml import etree
import cPickle
from sys import path

def get_cities(area):
    x = urllib2.urlopen("http://www.craigslist.org/about/sites")
    tree = etree.HTML(x.read())
    continents = tree.xpath("//div[@class='colmask']")
    search_continent = None
    for continent in continents:
        c_name = continent.find(".//h1[@class='continent_header']/a")
        if c_name.get('name') == area:
            search_continent = continent
            break

    cities = search_continent.xpath('.//div[@class="colleft"]//li/a')
    return [city.get('href') for city in cities]

if __name__ == '__main__':
    pkfile = open("{0}//city_pickle.pickle".format(path[0]), 'wb')
    cPickle.dump(get_cities('US'), pkfile)
    pkfile.close()