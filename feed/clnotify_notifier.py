"""
The Notifer module. This will send notifications
"""
import smtplib
import imaplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from common.clnotify_log import write_log


def login_email(protocol, user, password, host, port, ssl=True):
    """
    This will login into the email account
    and return the email object. It will use SSL by default

    @param protocol: imap or smtp
    @type protocol: string
    @type user: string
    @param user: username for the email login
    @type password: string
    @param password: password for the email account
    @type host: string
    @param host: email host address
    @type port: int
    @param port: port to connect at
    @param ssl: ssl enabled or not
    @type ssl: bool
    @rtype: smtplib() or imaplib()
    @return: the logged in mail obj
    """
    try:
        if protocol == 'imap' and ssl:
            mail = imaplib.IMAP4_SSL(host, port)
        elif protocol == 'imap' and not ssl:
            mail = imaplib.IMAP4(host, port)
        elif protocol == 'smtp' and ssl:
            mail = smtplib.SMTP(host, port)
            mail.starttls()
        elif protocol == 'smtp' and not ssl:
            mail = smtplib.SMTP(host, port)
            mail.ehlo()
        else:
            mail = None
            raise AttributeError(protocol, ssl)
        mail.login(user, password)
    except (smtplib.SMTPException, imaplib.IMAP4.error) as loginex:
        print "Error in Login: {0}".format(str(loginex))
        write_log('error_login', "Error in Login: {0} Type:{1} Args:{2}".format(str(loginex),
                                                                                type(loginex),
                                                                                str(loginex.args)))
        return None
    return mail


def send_email(mail, toaddrs, fromaddr, subject, message, log=None):
    """
    Send email function. Requires an already logged in
    mail object

    @param mail: logged in smtplib() object
    @type mail: smtplib()
    @param toaddrs: destination email(s)
    @type toaddrs: comma separated list of emails
    @param fromaddr: sender email address
    @type fromaddr: string
    @param subject: subject line of the email
    @type subject: string
    @type message: string
    @param message: The message you want to send
    @param log: any details to log
    @type log: string
    """
    mimemsg = MIMEMultipart()
    mimemsg['From'] = fromaddr
    mimemsg['To'] = toaddrs
    mimemsg['Subject'] = subject
    mimemsg.attach(MIMEText(message))
    recipients = [r for r in toaddrs.split(',')]
    try:
        mail.sendmail(fromaddr, recipients, mimemsg.as_string())
        # Logs
        write_log('notifications', str(recipients), log)
        return True
    except smtplib.SMTPException as mailex:
        write_log('error_smtp', "Error in Send Mail: {0} Type:{1} Args:{2}".format(str(mailex),
                                                                                   type(mailex),
                                                                                   str(mailex.args)))
        print "Error in Send Mail: {0}".format(str(mailex))
        return False
