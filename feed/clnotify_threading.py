"""
This will handle the multi-threading
portion of the application through Queues
"""
import multiprocessing as mp
from mechanize._urllib2 import HTTPError, URLError
import mechanize
import cookielib
from clnotify_log import write_log, write_exception_log, pickle_dump, pickle_load
from clnotify_parser import parse
import config
from sys import exc_info


def build_browser(city):
    """
    Builds a browser agent that will
    return a list of mechanize browser
    objects built for city supplied.
    Basically having the correct
    reffer string for each city
    """
    browser = mechanize.Browser()
    browser.set_handle_equiv(True)
    browser.set_handle_gzip(True)
    browser.set_handle_redirect(True)
    browser.set_handle_referer(True)
    browser.set_handle_robots(False)

    # Adding Cookies support
    clcookiejar = cookielib.LWPCookieJar(filename='clcookies')
    browser.set_cookiejar(clcookiejar)

    # Debug Support
    browser.set_debug_http(False)
    browser.set_debug_redirects(True)

    # Adding Headers
    browser.addheaders = [("User-Agent", config.headers['User-agent']),
                          ("Referer", config.headers['Referer'].format(city=city)),
                          ("Host", config.headers['Host'].format(city=city))]
    return browser


class Results(object):
    """
    simple access to results
    """
    def __init__(self):
        self.result = None

    def get_results(self):
        """
        Get Pickled Results
        """
        self.result = pickle_load(config.RESULT_PICKLE)
        if not self.result:
            # no file existing or pickle file corrupt
            # Clear the file, read it again and return it
            pickle_dump(set(), config.RESULT_PICKLE)
            return pickle_load(config.RESULT_PICKLE)
        else:
            return self.result

    def set_results(self, value):
        """
        Pickle Results for future use
        """
        self.result = value
        pickle_dump(self.result, config.RESULT_PICKLE)

    results = property(get_results, set_results)


def _make_request(browser, url):
    """
    Make a Request to the URL provided
    Return the result
    """
    try:
        content = browser.open(url)
    except (HTTPError, URLError):
        write_exception_log(exc_info(), 'http_url_error.log')
        write_log('request.log', 'Could not complete request for:{0}'.format(url))
        return ''
    # Todo check if its a Valid Response or NOT
    write_log('request.log', 'Request Completed For: {0}'.format(url))
    return content.read()


def _get_posts(task_queue, result_queue):
    """
    Gets the URL response, then parses it and
    finally returns the Title and URL of matching objects
    """
    while True:
        task = task_queue.get()
        if task is None:
            break
        url = task['url']
        city = task['city']
        browser = build_browser(city)
        response = _make_request(browser, url)
        parsed_response = parse(response)
        for response in parsed_response:
            notify_list = "City: {city} Post Title: {post}, Link: {url}".format(
                city=city, post=response['title'], url=response['url']
            )
            result_queue.put(notify_list)
        result_queue.put(None)


def start(tasks):
    """
    Start the process
    """
    #_result = Results()
    task_ctr = 1
    proc_ctr = 0
    cur_results = set()
    _result = Results()

    task_queue = mp.Queue()
    result_queue = mp.JoinableQueue()

    for task in tasks:
        task_queue.put(task)

    while proc_ctr <= config.NO_OF_PROCS:
        proc = mp.Process(target=_get_posts, args=(task_queue, result_queue))
        proc.start()
        proc_ctr += 1
    proc_ctr = 0

    while task_ctr <= len(tasks):
        info = result_queue.get()
        if info is None:
            task_ctr += 1
        cur_results.add(info)
        result_queue.task_done()

    while proc_ctr <= config.NO_OF_PROCS:
        task_queue.put(None)
        proc_ctr += 1

    task_ctr = 1
    result_queue.join()
    task_queue.close()
    result_queue.close()
    previous_results = _result.results
    new_results = cur_results.difference(previous_results)
    _result.results = cur_results
    return new_results
