"""
Feed Parser
"""
import feedparser
from clnotify_log import write_log


def parse(response):
    """
    Parse  the CSS using CSS Feeds
    """
    feeds = feedparser.parse(response)
    if feeds['bozo']:
        write_log('feedparser_error', feeds['bozo_exception'])
        return []
    else:
        return [({'title': post.title, 'url': post.link}) for post in feeds.entries]