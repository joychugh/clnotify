"""
This will handle the multi-threading
portion of the application through Queues

This will work the the files with suffix _search as this does searches and gets the results
This DOES NOT DO Feed Parsing
"""
import threading
import Queue
import cookielib
from logging import getLogger
from datetime import datetime

from mechanize._urllib2 import HTTPError, URLError
import mechanize
from parsers.search_parser import parse
from config import search_config as config


MAX_THREADS = 2
RESULTS = list()


def build_browser(city):
    """
    Builds a browser agent that will
    return a list of mechanize browser
    objects built for city supplied.
    Basically having the correct
    reffer string for each city
    """
    browser = mechanize.Browser()
    browser.set_handle_equiv(True)
    browser.set_handle_gzip(True)
    browser.set_handle_redirect(True)
    browser.set_handle_referer(True)
    browser.set_handle_robots(False)

    # Adding Cookies support
    clcookiejar = cookielib.LWPCookieJar(filename='clcookies')
    browser.set_cookiejar(clcookiejar)

    # Debug Support
    browser.set_debug_http(False)
    browser.set_debug_redirects(False)

    # Adding Headers
    browser.addheaders = [("User-Agent", config.headers['User-agent']),
                          ("Referer", config.headers['Referer'].format(city=city)),
                          ("Host", config.headers['Host'].format(city=city))]
    return browser


def _make_request(browser, url):
    """
    Make a Request to the URL provided
    Return the result
    """
    logger = getLogger('main.clnotify_run._make_request')
    try:
        content = browser.open(url)
    except (HTTPError, URLError, Exception):
        logger.exception("Could not make Request")
        #write_exception_log(exc_info(), 'http_url_error.log')
        #write_log('request.log', 'Could not complete request for:{0}'.format(url))
        return ''
    # Todo check if its a Valid Response or NOT
    #write_log('request.log', 'Request Completed For: {0}'.format(url))
    return content.read()


class MakeRequests(threading.Thread):
    """
    Make Requests Class
    """
    def __init__(self, task_queue, result_queue):
        """
        Initialize
        """
        threading.Thread.__init__(self)
        self.task_queue = task_queue
        self.result_queue = result_queue
        self.logger = getLogger('main.clnotify_run.make_request')

    def run(self):
        """
        Make the requests
        """
        self.logger.debug("Make Request Thread Started")
        while True:
            task = self.task_queue.get()
            self.logger.debug("Got Task From Queue")
            url = task['url']
            city = task['city']
            browser = build_browser(city)
            request_start_time = datetime.now()
            self.logger.debug("Making Request")
            response = _make_request(browser, url)
            request_end_time = datetime.now()
            request_duration = request_end_time - request_start_time
            log_messg = "Time Taken for Request for {url} is {time} ".format(url=url, time=request_duration.total_seconds())
            self.logger.debug(log_messg)
            response = {'response': response, 'city': city}
            self.result_queue.put(response)
            self.task_queue.task_done()


class ParseResponse(threading.Thread):
    """
    Parse Response from the result
    """
    def __init__(self, result_queue):
        """
        Initialize the Parse Response thread
        """
        threading.Thread.__init__(self)
        self.result_queue = result_queue
        self.logger = getLogger('main.clnotify_run.ParseResponse')

    def run(self):
        """
        Start Parsing
        """
        self.logger.debug("Starting Parsing Thread")
        global RESULTS
        while True:
            task = self.result_queue.get()
            self.logger.debug("Got Task From Result Queue")
            city = task['city']
            response = task['response']
            parsed_response = parse(response, config.CL_PARSE_PATTERN)
            self.logger.debug("parsed a response")
            for response in parsed_response:
                notify_list = "City: {city} Post Title: {post}, Link: {url}".format(
                    city=city, post=response['title'].encode('ascii', 'ignore'), url=response['url']
                )
                RESULTS.append(notify_list)
                self.logger.debug("Appended the response to Global Results")
            self.result_queue.task_done()


def start_threads(keyword_queue, keyword_result):
    """
    Start the process
    """
    logger = getLogger('main.search_clnotify_run')
    global RESULTS
    while True:
        tagged_task = keyword_queue.get()
        logger.debug("Got Task from Keyword Queue")
        if tagged_task is None:
            logger.info("Got Kill Signal from main")
            break
        tasks = tagged_task['tasks']
        keyword = tagged_task['keyword']
        log_messg = "Started Process for {0}".format(keyword)
        logger.info(log_messg)
        no_of_threads = MAX_THREADS
        thread_count_log = "Number of Threads = {0}".format(no_of_threads)
        logger.info(thread_count_log)
        thread_ctr = 0
        task_queue = Queue.Queue(300)
        result_queue = Queue.Queue()

        logger.info("Starting Request threads")
        while thread_ctr < no_of_threads:
            thread = MakeRequests(task_queue, result_queue)
            debug_messg = "Thread Number {0} started for Making Requests".format(thread_ctr)
            logger.debug(debug_messg)
            thread.setDaemon(True)
            thread.start()
            thread_ctr += 1
        thread_ctr = 0

        logger.info("Putting Tasks In Queue")
        for task in tasks:
            logger.debug("Putting task in Queue")
            task_queue.put(task)

        logger.info("Starting Parsing Threads")
        while thread_ctr < no_of_threads:
            thread = ParseResponse(result_queue)
            debug_messg = "Thread Number {0} started for Parsing".format(thread_ctr)
            logger.debug(debug_messg)
            thread.setDaemon(True)
            thread.start()
            thread_ctr += 1
        thread_ctr = 0
        task_queue.join()
        logger.info("Joined Task Queue")
        result_queue.join()
        logger.info("Joined Result Queue")
        tagged_results = {'keyword': keyword, 'result': RESULTS}
        keyword_result.put(tagged_results)
        info_log = "Keyword {0} completed processing and put in Queue for Main to extract".format(keyword)
        logger.info(info_log)
        RESULTS = list()

