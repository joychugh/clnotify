"""
Search Results Parser
"""
from lxml import etree
from common.clnotify_log import write_exception_log
from sys import exc_info


def extract_post_info(post):
    """
    Extract the price, Title and URL of the
    post using the price and title_url xpath
    """
    #price_item = post.find(".//span[@class='itempp']")
    #location_item = post.find(".//span[@class='itempn']/font)
    post_info = post.find('.//a')
    title = post_info.text
    url = post_info.get('href')
    return {'title': title, 'url': url}


def parse(response, parameter):
    """
    Parse the contents of the response
    according to the parameters provided
    in XPATH format.
    """
    try:
        html = etree.HTML(response)
        posts = html.xpath(parameter)
        results = [extract_post_info(post) for post in posts]
        return results
    except Exception:
        write_exception_log(exc_info(), 'parser_error')
        return []