"""
Get Cities from Pickle
"""
from clnotify_log import pickle_load
from logging import getLogger


def _get_cities():
    cities = [
        'http://nyc.craigslist.com',
        'http://boston.craigslist.com'
    ]
    logger = getLogger('search_clnotify_main.city')
    logger.warn('Using only NYC and Boston')
    logger.info('Using NYC and Boston')
    return cities


def get_cities():
    logger = getLogger('search_clnotify_main.city')
    logger.info('Getting Cities')
    return pickle_load('city_pickle')
