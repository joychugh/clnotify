"""
Return the Keywords from the Excel File
"""
from openpyxl import load_workbook
from openpyxl.shared.exc import InvalidFileException
from sys import path as syspath, exc_info
from clnotify_log import write_exception_log, write_log
from os import path as ospath


def get_keywords(filename, sheetname):
    """
    Return a list of keywords from the file name
    specified
    """
    file_name = "{filename}.xlsx".format(filename=filename)
    file_path = ospath.join(syspath[0], 'keywords', file_name)
    keyw_sh = None
    try:
        keyw_wb = load_workbook(file_path, use_iterators=True)
        keyw_sh = keyw_wb.get_sheet_by_name(name=sheetname)
    except InvalidFileException:
        write_exception_log(exc_info(), 'pyxl_error')

    if not keyw_sh:
        write_log('pyxl_sheet', 'Invalid Sheet Name ', sheetname)
        return []
    return [str(row[0].internal_value) for row in keyw_sh.iter_rows()]
