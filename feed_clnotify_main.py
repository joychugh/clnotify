"""
This uses Craigslist Feeds to search for new posts.
It is Unreliable.
"""
from config import feed_config as config
import timeit
from feed.clnotify_threading import start as start_process
from common.clnotify_notifier import login_email as login
from common.clnotify_notifier import send_email
from common import clnotify_city
from sys import exc_info
from urllib import urlencode
from common.clnotify_log import write_exception_log
import cProfile
from re import compile as re_compile
from common.clnotify_log import pickle_dump
from sys import path as syspath


def _build_search_q(city_url, keyword):
    """
    Build the search Query
    """
    query_params = {'query': keyword, 'srchType': 'T', 'format': 'rss'}
    query = urlencode(query_params)
    get_request = config.searchq.format(host=city_url, query=query)
    return get_request


def _build_task(city_url, keyword):
    """
    Build the Task with the browser and the URL
    """
    city_re = re_compile('//(\w*)')
    city = city_re.search(city_url)
    try:
        city = city.group(1)
    except AttributeError:
        write_exception_log(exc_info(), 'city_regex_error')
        city = 'Could Not Detect'
    search_q = _build_search_q(city_url, keyword)
    task_params = [search_q, city, keyword]
    task = dict(zip(config.TASK_FORMAT, task_params))
    return task


def _send_email_notification(results):
    """
    Get the results, create a message, and
    send the notifications
    """
    mail_obj = login('smtp', config.EMAIL['username'], config.EMAIL['password'],
                     config.EMAIL['smtp_server'], config.EMAIL['smtp_port'])
    results = "\n".join(result for result in results)
    email_body = config.EMAIL_FORMAT.format(notifications=results)
    if not send_email(mail_obj, config.EMAIL_NOTIFY_LIST, config.EMAIL['address'], config.EMAIL_SUBJECT, email_body):
        # Email not sent, do a pickle dump of data so that we dont lose it
        pickle_dump('emails_not_sent', results)
    else:
        return True


def start():
    """
    Starts the whole process
    """
    city_urls = clnotify_city.get_cities()
    keywords = config.keywords
    tasks = list()
    for city_url in city_urls:
        for keyword in keywords:
            tasks.append(_build_task(city_url, keyword))
    print tasks
    #results = start_process(tasks)
    #if results:
    #    _send_email_notification(results)
    #else:
    #    print "No new notifications"


if __name__ == '__main__':
    cProfile.run('start()', syspath[0]+ "\\prof")
