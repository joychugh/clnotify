"""
The Entry Point for the program.
This will handle all the communication between the
rest of the modules.
"""
import config.search_config as config
#import timeit
from os import path as ospath
from time import gmtime, strftime
from re import compile as re_compile
from search.clnotify_run import start_threads
from common.clnotify_notifier import login_email as login, send_email
from common import clnotify_city
from urllib import urlencode
from cProfile import run as cProfileRun
from common.clnotify_log import pickle_dump, write_exception_log, pickle_load, create_data_dir
from sys import path as syspath, exc_info
from common.clnotify_keywords import get_keywords
import multiprocessing as mp
from hashlib import md5
import logging
import logging.config


class Results(object):
    """
    simple access to results
    """
    def __init__(self, keyword):
        self.result = None
        self.filename = self.get_filename(keyword)

    @staticmethod
    def get_filename(keyword):
        """
        Generate the md5 hex for filename
        """
        hash_obj = md5(keyword)
        return hash_obj.hexdigest()

    def get_results(self):
        """
        Get Pickled Results
        """
        self.result = pickle_load(self.filename)
        if not self.result:
            # no file existing or pickle file corrupt
            # Clear the file, read it again and return it
            pickle_dump(set(), self.filename)
            return pickle_load(self.filename)
        else:
            return self.result

    def set_results(self, value):
        """
        Pickle Results for future use
        """
        self.result = value
        pickle_dump(self.result, self.filename)

    results = property(get_results, set_results)


def _build_search_q(city, keyword):
    """
    Build the search Query
    """
    host_part = city
    query_params = {'query': keyword, 'srchType': 'T', 'minAsk': '', 'maxAsk': ''}
    query = urlencode(query_params)
    get_request = config.searchq.format(host=host_part, query=query)
    return get_request


def _build_task(city_url, keyword):
    """
    Build the Task with the browser and the URL
    """
    city_re = re_compile('//(\w*)')
    city = city_re.search(city_url)
    try:
        city = city.group(1)
    except AttributeError:
        write_exception_log(exc_info(), 'city_regex_error')
        city = 'Could Not Detect'
    search_q = _build_search_q(city_url, keyword)
    task_params = [search_q, city, keyword]
    task = dict(zip(config.TASK_FORMAT, task_params))
    return task


def _send_email_notification(subject, content):
    """
    Get the results, create a message, and
    send the notifications
    """
    mail_obj = login('smtp', config.EMAIL['username'], config.EMAIL['password'],
                     config.EMAIL['smtp_server'], config.EMAIL['smtp_port'])
    results = "\n".join(result for result in content)
    email_body = config.EMAIL_FORMAT.format(notifications=results)
    if not send_email(mail_obj, config.EMAIL_NOTIFY_LIST, config.EMAIL['address'], subject, email_body):
        # Email not sent, do a pickle dump of data so that we dont lose it
        time = strftime('%d-%b-%Y %H:%M:%S', gmtime())
        file_name = "{datetime}-email-not-sent".format(datetime=time)
        pickle_dump(file_name, results)
        return False
    else:
        return True


def _get_new_results(tagged_results, result_files):
    """
    When passed the current result,
    it returns a set of new results while omitting the
    old posts.
    """
    all_new_results = list()
    for tagged_result in tagged_results:
        results = set()
        result = tagged_result['result']
        keyword = tagged_result['keyword']
        keyword_result_file = result_files[keyword]
        for posts in result:
            results.add(posts)
        previous_results = keyword_result_file.results
        new_results = results.difference(previous_results)
        keyword_result_file.results = results
        if new_results:
            all_new_results.append({'keyword': keyword, 'results': new_results})
    return all_new_results


def _send_notification(notify_type, results):
    """
    Send the notifications accroding to the notify_type
    mentioned email, or text etc
    """
    key_counter = 0
    subject = str()
    content = set()
    for result in results:
        subject = subject + ' ' + result['keyword']
        content = content.union(result['results'])
        key_counter += 1
        if key_counter >= config.KEYWORDS_PER_EMAIL:
            print subject
            #_send_email_notification(subject, content)
            subject = str()
            content = set()
            key_counter = 0
    if subject and content:
        print subject
        #_send_email_notification(subject, content)


def start():
    """
    Starts the whole process
    """
    cpu_count = int(mp.cpu_count())
    task_queue = mp.Queue()
    result_queue = mp.JoinableQueue()
    proc_ctr = 0
    all_results = list()
    city_urls = clnotify_city.get_cities()
    keywords = get_keywords('keywords', 'keywords')
    no_of_processes = cpu_count if len(keywords) > cpu_count else len(keywords)
    tasks = list()
    result_files = dict()
    for keyword in keywords:
        result_files[keyword] = Results(keyword)
        for city_url in city_urls:
            tasks.append(_build_task(city_url, keyword))
        tagged_task = {'keyword': keyword, 'tasks': tasks}
        task_queue.put(tagged_task)
        tasks = list()

    while proc_ctr < no_of_processes:
        proc = mp.Process(target=start_threads, args=(task_queue, result_queue))
        proc.start()
        proc_ctr += 1

    proc_ctr = 0

    for keyword in xrange(len(keywords)):
        tagged_result = result_queue.get()
        all_results.append(tagged_result)
        result_queue.task_done()

    while proc_ctr < no_of_processes:
        task_queue.put(None)
        proc_ctr += 1

    result_queue.join()
    task_queue.close()
    result_queue.close()
    new_results = _get_new_results(all_results, result_files)
    _send_notification('email', new_results)


if __name__ == '__main__':
    log_config = ospath.join(syspath[0], 'config', 'logconf.conf')
    logging.config.fileConfig(log_config)
    logger = logging.getLogger('search_clnotify_main')
    create_data_dir('keywords')
    create_data_dir('logs')
    create_data_dir('pickle')
    create_data_dir('performance')
    logger.info("Created Directories: Starting Process")
    import time
    logger.info("Starting CL Scanning Process")
    for i in xrange(1):
        filename = "{number}-perf".format(number=i)
        profile_path = ospath.join(syspath[0], 'performance', filename)
        cProfileRun('start()', profile_path)
        time.sleep(0)

