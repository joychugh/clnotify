#!/usr/bin/env python


# User Agent
headers = {'User-agent': "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:14.0) Gecko/20100101 Firefox/14.0.1",
              'Referer':    "http://{city}.craigslist.org/cta",
              'Host' :      "{city}.craigslist.org"}
searchq = "http://{city}.craigslist.org/search/cta?query=mercedes+280&srchType=T&minAsk=&maxAsk="

city = ['nyc', 'boston', 'losangeles']

PROCS_PER_TASK = 1
NO_OF_PROCS = 4

#CL_PARSE_PATTERN = "//html/body/blockquote[2]/p"
CL_PARSE_PATTERN = ".//xhtml:div[@class='entry']"

RESULT_PICKLE = 'pickled_results'

TASK_FORMAT = [
    'url',
    'xpath',
    'city'
]

keywords = ['"Porsche 356" | "356 Porsche" | 356a | 356b | 356c | 356sc']

search_query = "http://{city}.craigslist.org/search/cta?"
search_query_ = "http://boston.craigslist.org/search/cta?query=mercedes&srchType=T&format=rss"

# Email Stuff
EMAIL = {
    'username' : 'user_name',
    'password' : 'password',
    'smtp_server': 'smtp.gmail.com',
    'smtp_port': 587,
    'address': 'user_name@gmail.com'
}

EMAIL_NOTIFY_LIST = 'person2notify@email.com,anotherperson2notify@doge.com'

EMAIL_SUBJECT = "Your CL Notifications"
EMAIL_FORMAT = """
Hi,

Bellow are your notifications:
{notifications}

Thanks,
CL Notify
"""

# We can also build a list of cities through search/create_cities_pickle.py
config_params = {
    'headers': {
        'User-agent': "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:14.0) Gecko/20100101 Firefox/14.0.1",
        'Referer':    "http://{city}.craigslist.org/cta",
        'Host' :      "{city}.craigslist.org"
    },
    'searchq' :         "http://{city}.craigslist.org/search/cta?query=mercedes+280&srchType=T&minAsk=&maxAsk=",
    'city':             ['nyc', 'boston', 'losangeles'],
    'PROCS_PER_TASK':   1,
    'CL_PARSE_PATTERN': "//html/body/blockquote[2]/p",
    'RESULT_PICKLE' :   'pickled_results',
    'TASK_FORMAT' : ['url', 'xpath', 'city']
}
#pickle_dump(config_params, 'config_pickle')
